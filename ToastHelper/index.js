
import { NativeModules } from 'react-native';

const { RNToastHelper } = NativeModules;

export default RNToastHelper;

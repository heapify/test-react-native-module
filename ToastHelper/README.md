
# react-native-toast-helper

## Getting started

`$ npm install react-native-toast-helper --save`

### Mostly automatic installation

`$ react-native link react-native-toast-helper`

### Manual installation


#### iOS

1. In XCode, in the project navigator, right click `Libraries` ➜ `Add Files to [your project's name]`
2. Go to `node_modules` ➜ `react-native-toast-helper` and add `RNToastHelper.xcodeproj`
3. In XCode, in the project navigator, select your project. Add `libRNToastHelper.a` to your project's `Build Phases` ➜ `Link Binary With Libraries`
4. Run your project (`Cmd+R`)<

#### Android

1. Open up `android/app/src/main/java/[...]/MainActivity.java`
  - Add `import com.reactlibrary.RNToastHelperPackage;` to the imports at the top of the file
  - Add `new RNToastHelperPackage()` to the list returned by the `getPackages()` method
2. Append the following lines to `android/settings.gradle`:
  	```
  	include ':react-native-toast-helper'
  	project(':react-native-toast-helper').projectDir = new File(rootProject.projectDir, 	'../node_modules/react-native-toast-helper/android')
  	```
3. Insert the following lines inside the dependencies block in `android/app/build.gradle`:
  	```
      compile project(':react-native-toast-helper')
  	```

#### Windows
[Read it! :D](https://github.com/ReactWindows/react-native)

1. In Visual Studio add the `RNToastHelper.sln` in `node_modules/react-native-toast-helper/windows/RNToastHelper.sln` folder to their solution, reference from their app.
2. Open up your `MainPage.cs` app
  - Add `using Toast.Helper.RNToastHelper;` to the usings at the top of the file
  - Add `new RNToastHelperPackage()` to the `List<IReactPackage>` returned by the `Packages` method


## Usage
```javascript
import RNToastHelper from 'react-native-toast-helper';

// TODO: What to do with the module?
RNToastHelper;
```
  